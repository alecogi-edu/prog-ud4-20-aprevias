# Actividades Previas UD4


[**Actividad3.-**](/src/es/coloma/Actividad3.java) Crea un método sin argumentos que al referenciarlo muestre todas las 
tablas de multiplicar. El tipo de retorno será void.

[**Actividad4.-**](/src/es/coloma/Actividad4.java) Escribe un método sin argumentos que devuelva un entero que corresponderá 
al mayor de tres números enteros. Los números enteros los pediremos al usuario en el propio método.

[**Actividad5.-**](/src/es/coloma/Actividad5.java) Crea un método con 1 argumento de tipo entero llamado "multiplicando" que al llamarlo muestre la tabla de multiplicar correspondiente. El tipo de retorno será void

[**Actividad6.-**](/src/es/coloma/Actividad6.java) Refactoriza el ejercicio 4 para que los números enteros se
pidan en la función main y se pasen como argumento a la función creada.

[**Actividad7.-**](/src/es/coloma/Actividad7.java) Escribe un método que calcule la potencia de un número real,
elevado a un número entero. Referencia-lo para los valores 10^2, 10^-2, 2^4, 2^1.

[**Actividad15.-**](/src/es/coloma/actividad15/Actividad15.java) Implementa y compila el siguiente ejemplo. Añade un método a la clase Matematica llamado esPrimo que dado un entero long pasado como argumento, nos indique si se trata de un número primo.
Un número primo es aquel que solo es divisible por el mismo y la unidad
Crea un programa que, utilizando la clase Matematica, pida números al usuario de forma indefinida e indique si son primos o no. 

```java

public class Matematica {

    public static int suma(int a, int b) {
        return a + b;
    }

    public static int suma(int a, int b, int c) {
        return a + b + c;
    }
}

public class MyApp {

    public static void main(String[] args) {
        int result1 = Matematica.suma(12,15); int result2 = Matematica.suma(45,50);
    }

}

```

[**Actividad18.-**](/src/es/coloma/Actividad18.java) Dado el siguiente programa en Java que muestra un menú por consola.


<img src="/resources/actividad18.png" width="480px">


Refactorízalo para que cumpla con las siguientes cabeceras y el diseño propuesto


```java
public class Actividad18 {

    public static final int OPCION_SALUDAR = 1;
    public static final int OPCION_COMER = 2;
    public static final int OPCION_HABLAR = 3;
    public static final int OPCION_SALIR = 4;

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {

        mostrarMenu();

    }
    /**
     *  Problema General: 1.- Muestra el menú y gestiona la opción seleccionada por el usuario
     *  Repetira la acción hasta que la opción introducida sea salir
     **/
    public static void mostrarMenu() {
        ...
        opcionSeleccionada = seleccionarOpcion();
        manejarOpcion(opcionSeleccionada);
        ...
    }
    /**
     *  1er Nivel 1.1.- Muestra el menú y pregunta al usuario para que seleccione una opción
     *  @return int opción seleccionada por el usuario
     **/
    private static int seleccionarOpcion() {
        ...
    }
    /**
     *  1er Nivel 1.2.- Realiza las tareas asociadas a la opción seleccionada
     *  @param opcionSeleccionada
     **/
    private static void manejarOpcion(int opcionSeleccionada){
        ...
    }

}

````


