package es.coloma;

/**
 *  Actividad 5.- Crea un método con 1 argumento de tipo entero llamado "multiplicando"
 *  que al llamarlo muestre la tabla de multiplicar correspondiente.
 *  el tipo de retorno será void.
 */
public class Actividad5 {

    public static void main(String[] args) {

        mostrarTabla(1);
        mostrarTabla(3);
        mostrarTabla(8);
        mostrarTabla(5);

    }

    public static void mostrarTabla(int multiplicando) {

        System.out.printf("Tabla del %d %n", multiplicando);

        for (int j = 1; j <=10; j++) {
            System.out.printf(" %d X %d = %d %n", multiplicando ,
                    j, multiplicando * j);
        }

        System.out.println("");

    }

}
