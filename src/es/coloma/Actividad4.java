package es.coloma;

import java.util.Scanner;

/**
 * Actividad 4.- Escribe un método sin argumentos que devuelva un
 * entero que corresponderá al mayor de tres números enteros. Los
 * números enteros los pediremos al usuario en el propio método.
 */
public class Actividad4 {

    public static void main(String[] args) {

        int mayor = obtenerMayor();
        System.out.println(mayor);

    }

    public static int obtenerMayor() {

        Scanner teclado = new Scanner(System.in);

        System.out.println("Introduce el número 1");
        int num1 = teclado.nextInt();

        System.out.println("Introduce el número 2");
        int num2 = teclado.nextInt();

        System.out.println("Introduce el número 3");
        int num3 = teclado.nextInt();

        int mayor = num1;
        if (num2 > mayor) {
            mayor = num2;
        }
        if (num3 > mayor) {
            mayor = num3;
        }
        return mayor;

    }

}
