package es.coloma;


/**
 * Actividad 4.9.- Escribe un método que devuelva el mayor de cuatro números pasados como parámetros.
 */
public class Actividad9 {

    public static void main(String[] args) {

        int num1 = 3;
        int num2 = 4;
        int num3 = 5;
        int num4 = 6;

        int mayor = dameElMayor(num1, num2, num3, num4);
        System.out.printf("El mayor de %d, %d, %d, %d es %d %n",
                num1, num2, num3, num4, mayor);

        num2 = 100;
        mayor = dameElMayor(num1, num2, num3, num4);
        System.out.printf("El mayor de %d, %d, %d, %d es %d %n",
                num1, num2, num3, num4, mayor);

        num3 = 60;
        mayor = dameElMayor(num1, num2, num3, num4);
        System.out.printf("El mayor de %d, %d, %d, %d es %d %n",
                num1, num2, num3, num4, mayor);

    }

    public static int dameElMayor(int num1, int num2, int num3, int num4) {

        int mayor = num1;
        if (num2 > mayor) {
            mayor = num2;
        }
        if (num3 > mayor) {
            mayor = num3;
        }
        if (num4 > mayor) {
            mayor = num4;
        }
        return mayor;

    }


}


