package es.coloma;

import java.util.Scanner;

public class Actividad18 {

    public static final int OPCION_SALUDAR = 1;
    public static final int OPCION_COMER = 2;
    public static final int OPCION_HABLAR = 3;
    public static final int OPCION_SALIR = 4;

    public static Scanner teclado = new Scanner(System.in);

    public static void main(String[] args) {

        mostrarMenu();

    }
    /**
     *  Problema General: 1.- Muestra el menú y gestiona la opción seleccionada por el usuario
     *  Repetira la acción hasta que la opción introducida sea salir
     **/
    public static void mostrarMenu() {

        int opcionSeleccionada;
        do {
            opcionSeleccionada = seleccionarOpcion();
            manejarOpcion(opcionSeleccionada);
        } while (opcionSeleccionada != OPCION_SALIR);

    }
    /**
     *  1er Nivel 1.1.- Muestra el menú y pregunta al usuario para que seleccione una opción
     *  @return int opción seleccionada por el usuario
     **/
    private static int seleccionarOpcion() {

        System.out.printf("%d. Saludar %n", OPCION_SALUDAR);
        System.out.printf("%d. Comer %n", OPCION_COMER);
        System.out.printf("%d. Hablar %n", OPCION_HABLAR);
        System.out.printf("%d. Salir %n", OPCION_SALIR);
        return teclado.nextInt();

    }
    /**
     *  1er Nivel 1.2.- Realiza las tareas asociadas a la opción seleccionada
     *  @param opcionSeleccionada
     **/
    private static void manejarOpcion(int opcionSeleccionada){

        switch (opcionSeleccionada) {
            case OPCION_SALUDAR:
                System.out.println("Hola");
                break;
            case OPCION_COMER:
                System.out.println("Comiendo...");
                break;
            case OPCION_HABLAR:
                System.out.println("En un lugar de la mancha...");
                break;
            case OPCION_SALIR:
                System.out.println("Adios...");
                break;
            default:
                System.out.println("Error");
        }

    }
}

