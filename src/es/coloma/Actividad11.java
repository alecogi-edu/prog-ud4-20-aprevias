package es.coloma;

/**
 * Actividad 4.11.- Escribe un método que acepte tres argumentos: un carácter y dos enteros.
 * El primer entero indica el número de veces que se imprimirá el carácter en la línea y el segundo entero indica el número de líneas que se han de imprimir.
 */
public class Actividad11 {

    public static void main(String[] args) {

        int numero = 10;
        imprime('c', numero, 4);
        imprime('a', 4, 4);
    }

    public static void imprime(char caracter, int numVeces, int numLineas) {

        for (int j = 0; j < numLineas ; j++) {
            for (int i = 0; i < numVeces ; i++) {
                System.out.printf("%s", caracter);
            }
            System.out.println();
        }
        System.out.println();

    }

}
