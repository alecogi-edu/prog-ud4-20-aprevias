package es.coloma;

import java.util.Scanner;

/**
 * Actividad 6.- Refactoriza el ejercicio 4 para que los números enteros se
 * pidan en la función main y se pasen como argumento a la función
 * creada.
 */
public class Actividad6 {

    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);

        System.out.println("Introduce el número 1");
        int num1 = teclado.nextInt();

        System.out.println("Introduce el número 2");
        int num2 = teclado.nextInt();

        System.out.println("Introduce el número 3");
        int num3 = teclado.nextInt();

        System.out.printf("El mayor de %d, %d, %d es %d",
                num1, num2, num3, obtenerMayor(num1, num2, num3));

    }

    public static int obtenerMayor(int num1, int num2, int num3) {

        int mayor = num1;
        if (num2 > mayor) {
            mayor = num2;
        }
        if (num3 > mayor) {
            mayor = num3;
        }
        return mayor;

    }

}
