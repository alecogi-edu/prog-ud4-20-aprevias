package es.coloma.actividad15;

public class MyApp {

    public static int suma(int a, int b) {
        return a + b;
    }

    public static int suma(int a, int b, int c) {
        return a + b + c;
    }

    public static boolean esPrimo(long number) {

        for (int i = 2; i < number; i++) {
            if ((number % i) == 0){
                return false;
            }
        }
        return true;

    }

}
