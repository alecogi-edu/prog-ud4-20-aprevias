package es.coloma.actividad15;

public class Actividad15 {

    public static void main(String[] args) {

        int result1 = MyApp.suma(12,14);
        System.out.printf("El resultado es: %d %n", result1);

        int result = MyApp.suma(12,14,18);
        System.out.printf("El resultado es: %d %n", result);

        System.out.printf("El numero 5 es primo: %b %n",
                MyApp.esPrimo(5));

        System.out.printf("El numero 10 es primo: %b %n",
                MyApp.esPrimo(10));

        System.out.printf("El numero 50 es primo: %b %n",
                MyApp.esPrimo(5));

        System.out.printf("El numero 15 es primo: %b %n",
                MyApp.esPrimo(15));


        int resultado = (MyApp.suma(2,3) + MyApp.suma(4,5));

    }

}
