package es.coloma;

/**
 * Actividad 4.10.- Escribe un método que acepte dos argumentos: el carácter que se desea imprimir y el número de veces que se ha de imprimir.
 *
 * public static void imprime(char caracter, int numVeces)
 *
 *
 * Ejemplo de ejecución para imprime('a', 20);
 * aaaaaaaaaaaaaaaaaaaa
 */
public class Actividad10 {

    public static void main(String[] args) {

        imprime('a', 60);
        imprime('*', 120);
        imprime('2', 1100);

    }

    public static void imprime(char caracter, int numVeces) {

        for (int i = 0; i < numVeces; i++) {
            System.out.print(caracter);
        }
        System.out.println();

    }

}
