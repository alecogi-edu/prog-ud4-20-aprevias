package es.coloma;

/**
 * Actividad 3.- Crea un método sin argumentos que al referenciarlo muestre todas las tablas de multiplicar.
 * El tipo de retorno será void.
 */
public class Actividad3 {

    public static void main(String[] args) {

        System.out.println("Primera Vez");
        mostrarTablas();

        System.out.println("\nSegunda Vez");
        mostrarTablas();

    }

    public static void mostrarTablas() {

        for (int i = 1; i <=10; i++) {
            System.out.printf("Tabla del %d \n", i);
            for (int j = 1; j <=10; j++) {
                System.out.printf(" %d X %d = %d \n", i , j, i * j);
            }
            System.out.println();
        }

    }

}
