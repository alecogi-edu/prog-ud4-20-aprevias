package es.coloma;

import java.util.Scanner;

/**
 * Activitat 7.- Escribe un método que calcule la potencia de un número real,
 * elevado a un número entero. Referencia-lo para los valores 10^2, 10^-2, 2^4, 2^1.
 */
public class Actividad7 {

    public static void main(String[] args) {

        System.out.printf("El resultado de 10^2 = %.3f \n", calcularPotencia(10,2));
        System.out.printf("El resultado de 10^-2 = %.3f \n", calcularPotencia(10,-2));
        System.out.printf("El resultado de 2^4 = %.3f \n", calcularPotencia(2,4));
        System.out.printf("El resultado de 2^1 = %.3f \n", calcularPotencia(2,1));

    }

    public static float calcularPotencia(float base, int exponente) {

        if (exponente == 0) {
            return 1;
        }else {
            float result = 1;
            for (int i = 1; i <= Math.abs(exponente); i++) {
                result *= base;
            }
            if (exponente < 0) {
                return 1 / result;
            }
            return result;
        }

    }

}
